// Written by Cenna van Manen.

//This macro will generate a large data set that can be used to determine the error of the cilia measurement script.
//Mind that there is sufficient disk space before starting the macro.

print ("Start generating test files");

print ("Pick one image with the correct metadata");
open();
input_path = File.directory;
print ("Input path:", input_path);
output_path = CreateOutputDirectory(input_path);
print ("Output path:", output_path);

path_blank_image = GenerateBlankFile(output_path);
DrawBars(path_blank_image, output_path);

SaveLog(output_path);
print("Finished generating test files");


//Create output directory
function CreateOutputDirectory(folder){
	parent_folder = File.getParent(folder);
	output_folder = parent_folder + File.separator + "Test set" + File.separator;
	File.makeDirectory(output_folder);
	return output_folder;
}

//Generate blank file with the original metadata of the images you want to analyze with the macro
function GenerateBlankFile(output_folder){
	run("Select All");
	setBackgroundColor(0, 0, 0);
	run("Clear", "slice");
	run("Select None");
	output_path = output_folder + "Blank image";
	saveAs(".tiff", output_path);
	return output_path;
}


//Draw the test images and save in a seperate folder
function DrawBars(input_image, output_folder){
	input_image = input_image + ".tif";
	output_folder = output_folder + "Images" + File.separator;
	File.makeDirectory(output_folder);

	getPixelSize(pixel_unit, pixel_width, pixel_height);
	getDimensions(image_width, image_height, image_channels, image_slices, image_frames);
	close();
	
	x1 = 0.5*image_width;
	y1 = 0.25*image_height;
	y2 = 0.5*image_height;
	y3 = 0.75*image_height;

	print ("Coordinates lines/squares: (" , x1 , ",", y1, "), (", x1, ",", y2, "), (", x1, ",", y3, ")" );

	setColor(255,255,255);
	setLineWidth(1);
	max_width_um = 10;
	max_width_pixel = (max_width_um/pixel_width)+1; //+1 to make sure it stops drawing after the max width is reached
	print ("Max width structures: ", max_width_pixel, "pixels");
	max_height_um = 1;
	max_height_pixel = (max_height_um /pixel_height)+1; //+1 to make sure it stops drawing after the max height is reached
	print ("Max height structures: ", max_height_pixel, "pixels");

	//structures smaller than 2 pixels are not drawn
	for (i=3;i<max_width_pixel ;i++){
		for (j=3;j<max_height_pixel;j++){
			//verticle rctangles are not drawn
			if (i<j){
				print (i, "x", j, "is not drawn, since it is a verticle rectangle");
			}
			else {
				//open(input_image);
				run("Bio-Formats (Windowless)", "open=[input_image]");
				fillRect(x1, y1, i, j);
				fillRect(x1, y2, i, j);
				fillRect(x1, y3, i, j);
				run("Flatten");
				run("8-bit");
				//draw horizontal recatngles
				if(i>j){
					output_path = output_folder + "_" + i + "x" + j + "_pixel_bars";
				}
				//draw squares
				if(i==j){
					output_path = output_folder + "_" + i + "x" + j + "_pixel_squares";
				}
				print ("Image with", i, "x", j, "structures is drawn and saved");
				saveAs(".tiff", output_path);
				close("*");
			}
		}
	}
	close("*");
}

//Saves the summary and the log file of the run
function SaveLog(output_folder){
	selectWindow("Log");
	saveAs("txt", output_folder +"Log.txt");
}