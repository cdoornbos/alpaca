// Written by Cenna van Manen.

// This macro can rearrange the channels and/or removes channels from all images in a specific folder.


// As input, the macro assumes all images in the chosen folders have the same number of channels and the channels are arranged in the same order.
// Furthermore, make sure the files and foldername do not contain comma's (,) or semicolons (;). Last, the indicated folder should not contain any other files.

// As output a new folder will be generated called 'Images with re-arranged channels' in which the new images will be stored.


print ("Starting analysis");
requires("1.52h")

user_directories = InOutputDirectory();
new_order = DetermineNewChannelOrder(user_directories);
RunSilentMode();
ProcessImages(user_directories[0],user_directories[1],new_order);
print ("Finished re-arranging channels");



//USER defines input folder and an output folder is generated automatically
function InOutputDirectory(){
	print ("Choose input folder");
	input_folder = getDirectory("Choose input folder");
	parent_folder = File.getParent(input_folder);
	output_folder = parent_folder + File.separator + "Images with re-arranged channels" 
	File.makeDirectory(output_folder);
	print("Input folder:", input_folder);
	print("Output folder:", output_folder);
	user_folders = newArray(input_folder, output_folder);
	return user_folders;
}

//USER defines the new order of the channels
function DetermineNewChannelOrder(folders){
	images = getFileList(folders[0]);
	input_path = folders[0] + images[0];
	run("Bio-Formats (Windowless)", "open=[input_path]");
	getDimensions(image_width, image_height, image_channels, image_slices, image_frames);
	for (i=0; i<image_channels; i++){
		number_to_add = i+1;
		default_number = default_number*10 + number_to_add; 
	}
	if (image_slices > 1){
		run("Z Project...", "projection=[Max Intensity]");
		close(images[0]);
	}
	make_montage_string = "columns=" +  image_channels + " rows=1 scale=0.25 font=24 label";
	run("Make Montage...", make_montage_string);
	Dialog.create("Re-order the channels");
	Dialog.addMessage("Indicate below the new order of channels. If you want to remove a channel, you can simply leave its channel number out."
	+ "\n" + "The channel numbers are indicated below the image. Mind that you indicate the new order only in numbers, without spaces or punctuation marks.");
	Dialog.addNumber("New order", default_number);
	Dialog.show();
	new_order = Dialog.getNumber();
	new_order_string = "new=" + new_order;
	print ("Previous order: ", default_number);
	print ("New order: ", new_order);
	close();
	close();
	return new_order_string;
}

//Define if the macro will be ran in silent mode
function RunSilentMode(){
	silent_mode = getBoolean("Do you want to run the macro in silent mode? This will not open images while running." 
	+ "\n" + "If you select 'no' you will not be able to use the computer while the macro is running. ");
	setBatchMode(silent_mode);
}

function ProcessImages (input_folder, output_folder, new_order){
	images = getFileList(input_folder);
	print ("Re-arranging the following images: ");
	for (k=0; k<images.length; k++){
		print ("    ", images[k]);
		input_path = input_folder + images[k];
		run("Bio-Formats (Windowless)", "open=[input_path]");
		run("Arrange Channels...", new_order);
		saveAs("Tiff", output_folder + File.separator + images[k]);
		close();
	}
}