// Written by Cenna van Manen.

// As input, the macro assumes all images in the chosen folder have the same number of channels and the channels are arranged in the same order.
// Furthermore, make sure the files and foldername do not contain comma's (,) or semicolons (;). Last, the indicated folder should not contain any other files.

// As output, separate folders will be generated to store the single plane tifs per channel, maintaining the original file names.

print ("Starting channel splitter.");
requires("1.52h")
 
//Determine if the macro is called upon by another macro, otherwise the variables will be gathered here
//Data format data_from_other_macro: input folder, general output folder, channel name list, channel names, image type, is silent mode
data_from_other_macro = getArgument();
if (lengthOf(data_from_other_macro)>1){
	data_from_other_macro = split(data_from_other_macro, ",");
	//Sets silent mode based on previously indicated preferences
	if(data_from_other_macro[4]){
		setBatchMode(1);
	}
	else {
		setBatchMode(0);
	}
	chosen_folder = data_from_other_macro[0];
	general_output_folder = data_from_other_macro[1];
	channel_names_list = data_from_other_macro[2];
	channel_names = split(channel_names_list, ";");
	image_type = data_from_other_macro[3];
	output_folders = CreateOutputDirectories(general_output_folder, channel_names);
	//Save the macro that was used
	File.copy(File.directory + File.name, general_output_folder + "Macro_used_to_split_channels.txt");
}

else {
	print ("Choose input folder");
	chosen_folder = getDirectory("Choose input folder");
	channel_names = GetChannelNames();
	RunSilentMode();
	output_folders = CreateOutputDirectories(chosen_folder, channel_names);
	general_output_folder = File.getParent(output_folders[0]) + File.separator;
	//Save the macro that was used
	File.copy(File.directory + File.name, general_output_folder + "Macro_used_to_split_channels.txt");
	image_type = GetInputType(chosen_folder);
}

SplitChannels(chosen_folder, output_folders, image_type);
print ("Splitting channels finished.");

//The user defines the number of channels and their names
function GetChannelNames(){
	Dialog.create("Give channel names");
	Dialog.addMessage("Define the names of the channels you want to split as they are in the original file. Make sure to: "
		+ "\n" + "-Supply the same amount of channels as are in the original file "
		+ "\n"+ "-Name the channels in the correct order as in the original file "
		+ "\n"+ "-Do not use any spaces. e.g. DAPI,488");
	Dialog.addString ("Channel names", "");
	Dialog.show();
	channel_names_input = Dialog.getString();
	channel_names = split(channel_names_input, ",");
	return channel_names;
}

//Create output directories
function CreateOutputDirectories(input_folder, channel_names){
	print ("Input_folder splitter: ", input_folder);
	parent_folder = File.getParent(input_folder);
	print ("Parent_folder splitter: ", parent_folder);
	main_output_folder = parent_folder + File.separator + "Split channels" + File.separator;
	output_folder_array = newArray(channel_names.length);
	File.makeDirectory(main_output_folder);
		for (i=0; i<channel_names.length; i++){
		output_folder = main_output_folder + channel_names[i]+ File.separator;
		File.makeDirectory(output_folder);
		output_folder_array[i] = output_folder;
		}
	return output_folder_array;
}

//The macro determines if the images are stacks or not, based on the first image in the file
function GetInputType(input_folder){
	images = getFileList(input_folder);
	input_path = input_folder + images[0];
	run("Bio-Formats (Windowless)", "open=[input_path]");
	getDimensions(image_width, image_height, image_channels, image_slices, image_frames);
	is_image_stack = image_slices > 1;
	if (is_image_stack){
		image_is_stack_text = "a stack";
	}
	else{
		image_is_stack_text = "no stack";
	}
	print("    This image is " + image_is_stack_text + ".");
	close();
	return is_image_stack;
}

//Define if the macro will be ran in silent mode
function RunSilentMode(){
	silent_mode = getBoolean("Do you want to run the macro in silent mode? This will not open images while running." 
	+ "\n" + "If you select 'no' you will not be able to use the computer while the macro is running. ");
	setBatchMode(silent_mode);
}

//Splits the channels and if the input images are stacks the planes are merged	
function SplitChannels(input_folder, output_folder_array, image_type){
	print ("Splitting channels and merging stacks:");
	images = getFileList(input_folder);
	for (j=0; j<images.length; j++){
		input_path = input_folder + images[j];
		run("Bio-Formats (Windowless)", "open=[input_path]");
		print ("    ", images[j]);	
		//Checks if the image has multiple channels to split, otherwise it will continue without splitting
		if (output_folder_array.length > 1){
			run("Split Channels");
		}
		windows = getList("image.titles");
		for (k=0; k<windows.length; k++){
			selectWindow(windows[k]);
			//If the image is a stack, the image will be merged into a single plane tif file
			if (image_type){
				run("Z Project...", "projection=[Max Intensity]");
				selectWindow(windows[k]);
				close();
			}
			//Checks if multiple channels are indicated to show the same structure, and if so, these channels are merged
			output_path = output_folder_array[k] + File.separator + images[j];
			output_path = replace(output_path, ".czi", ".tif");
			if (File.exists(output_path)){
				getPixelSize(unit, pixel_width, pixel_height);
				scale = 1/pixel_width;
				run("Bio-Formats (Windowless)", "open=[output_path]");
				
				//Since the 'Images to stack" function resets the scale, this failsafe should correct the scale
				run("Images to Stack", "name=Stack title=[] use");
				scale_set = "distance=" + scale + " known=1 pixel=1 unit=micron";
				run("Set Scale...", scale_set);
				
				run("Z Project...", "projection=[Max Intensity]");	
				selectWindow("Stack");
				close();
			}	
			saveAs("Tiff", output_path);	
			close();
		}
	}
}