// Written by Cenna van Manen.

// This macro should be placed in the folder: "...\Fiji.app\macros\toolsets" 
// to generate a "Cilia Tools" bar, which allows easy access to the different macros and help document.

//Run the cilia master macro
macro "Whole cilia analysis Action Tool - B00 Cceg V4bb4 Cbdf F1eg5 V41a6 C379 L2h2b L3b38 L3816 L1610 L2064 L6481 L81c1 Lc1e3 Lf3g0 Lg0g6 Lg6f9 Lfagb Lgbgh O51a6 L3b6f L6fef Lefgb C057 L1h1b L2b28 L2806 L0600 L1054 L5471 L71b1 Lb1d3 Le3g0 Lh0h6 Lh6e9 Leafb Lfbfh C179 O41a6 L2b5f L5fdf Ldffb C134 F3832 Fc832 L7c9c L8d8e F7f42 C9bd D59"{
	runMacro ("1 Master measurement macro.ijm");
}

/*
macro "Whole cilia analysis Action Tool - C057 B08 T4716M T5716M"{
	runMacro ("1 Master measurement macro.ijm");
}
*/

//Run the channel splitter macro
macro "Channel splitter Action Tool - B02 Cf00 F0088 C0c0 F4488 C00f F8888 "{
	runMacro ("2 Channel splitter.ijm");
}

//Run counting nuclei macro
macro "Count nuclei Action Tool - C057 B08 T4716# T5716#"{
	runMacro ("3 Counting nuclei.ijm");
}

//Run measure cilia macro
macro "Measure cilia Action Tool - C057 L28d8 L29d9 L242d Ld4dd"{
	runMacro ("4 Measuring cilia length.ijm");
}

//Run IFT macro
macro "Analyze IFT accumulations Action Tool - C057 B08 T0612I T1612I T4612F T5612F Tb612T Tc612T"{
	runMacro ("5 Measuring IFT accumulations.ijm");
}

//Run rearrange channels macro
macro "Rearrange channels Action Tool - C057 B02 R00aa T39081 R66aa Tcf082"{
	runMacro ("6 Rearrange image channels.ijm");
}

//Help
macro "Get Help Action Tool - C057 B06 T4716? T5716? F7733"{
	path_macros = getDirectory("macros");
	path_help_file = path_macros + "7 Manual_Automated_Cilia_Measuring.pdf"+ File.separator
	open(path_help_file);
}
