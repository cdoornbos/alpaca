// Written by Cenna van Manen.

// This macro can prepare all images for complete cilia analysis. It will split the channels, merge stacks (if applicable) to generate flat .tif files and analyse each channel separately.
// The image preparation, nuclei count or cilia measurements can also be run separately by running the individual macros, see the manual for more information.
// Of note, the macro assumes all images in the chosen folder have the same number of channels and the channels are arranged in the same order.
// Furthermore, make sure the files and foldername do not contain comma's (,) or semicolons (;). Last, the indicated folder should not contain any other files.



print ("Starting analysis");
requires("1.52n")

user_directories = InOutputDirectory();
image_variabels = DetermineImageProperties(user_directories);
is_analyze_length = DetermineAnalysisType(image_variabels);
ProcessImages(user_directories, image_variabels, is_analyze_length);
SaveLog(user_directories[1]);
print ("Finished analysis");



//USER defines input folder and an output folder is generated automatically
function InOutputDirectory(){
	print ("Choose input folder");
	input_folder = getDirectory("Choose input folder");
	parent_folder = File.getParent(input_folder);
	output_folder = parent_folder + File.separator + "Analyzed images" 
	File.makeDirectory(output_folder);
	output_folder = output_folder + File.separator + "General" + File.separator;
	File.makeDirectory(output_folder);
	print("Input folder:", input_folder);
	print("Output folder:", output_folder);
	user_folders = newArray(input_folder, output_folder);
	return user_folders;
}

//Opens the first image in the input folder and determines if it is a stack or not, and the number of channels the image has. 
//The USER then indicates how different channels should be processed.
function DetermineImageProperties(folders){
	images = getFileList(folders[0]);
	input_path = folders[0] + images[0];
	run("Bio-Formats (Windowless)", "open=[input_path]");
	getDimensions(image_width, image_height, image_channels, image_slices, image_frames);
	is_image_stack = image_slices > 1;
	if (is_image_stack){
		image_is_stack_text = "a stack";
	}
	else{
		image_is_stack_text = "no stack";
	}
	image_properties_combined = newArray(1);
	image_properties_combined[0] = is_image_stack;
	analysis_methods = newArray("Nuclear marker", "Ciliary marker", "IFT marker", "Basal body marker", "Other");
	print("Image properties:");
	print("    This image is " + image_is_stack_text + ".");
	print("    Number of channels: " + image_channels + ".");
	run("Split Channels");
	images = getList("image.titles");
	for (i=0; i<image_channels; i++){
		selectWindow(images[i]);
		if (image_slices > 1){
			run("Z Project...", "projection=[Max Intensity]");
			close(images[i]);
		}
		Dialog.create("Indicate what is shown in each channel") 
		Dialog.addRadioButtonGroup("The channels will be shown one at a time, to indicate what they represent and how the macro will"
			+ "\n" + "analyze it. Therefore, all images in the input folder should have the same number of channels and the channels"
			+ "\n" + "should be in the same order. You can indicate multiple channels in the same way, these will then be merged"
			+ "\n" + "before analysis. Of note, channels indicated as 'Other' will only be split from the other channels and the"
			+ "\n" + "stacks will be flattend."
			,analysis_methods, lengthOf(analysis_methods), 1, "Ciliary marker");
		Dialog.show;
		user_defined_channel = Dialog.getRadioButton();
		if (user_defined_channel == "Other"){
			Dialog.create("Specify other");
			Dialog.addMessage("This channel was marked as 'Other', therefore indicate the name for this channel.")
			Dialog.addString ("Channel name: ", "");
			Dialog.show();
			channel_name = Dialog.getString();
			user_defined_channel = "Other (" + channel_name + ")";
		}
		image_properties_combined = Array.concat(image_properties_combined, user_defined_channel);
		channel_number = i+1;
		print ("    Channel " + channel_number + ": " + user_defined_channel);
		close();
	} 
	return image_properties_combined;
}

//USER defines if the macro will be ran in silent mode or not
function RunSilentMode(){
	silent_mode = getBoolean("Do you want to run the macro in silent mode? This will not open images while running." 
	+ "\n" + "If you select 'no' you will not be able to use the computer while the macro is running. ");
	setBatchMode(silent_mode);
	return silent_mode;
}

//USER defines if the cilia length will be measured as well if there is an IFT channel
function DetermineAnalysisType(image_properties) {
	image_properties = Array.slice(image_properties,1,lengthOf(image_properties));
	is_analyze_length = 1;
	for (i=0; i<lengthOf(image_properties); i++){
		if (image_properties[i] == "IFT marker" ){
			is_analyze_length = getBoolean("Would you like to analyze the cilia length and the IFT accumulations?", "Yes, analyze both", "No, only anlyze the IFT accumulations");
		}
	}
	return is_analyze_length;
}

//Processes the images using the channel splitter macro and different analysis macros for the indicated channels
function ProcessImages(folders, image_variables, is_analyze_length){
	is_silent_mode = RunSilentMode();
	for (j=1; j<image_variables.length; j++){
		if (j==1){
			channel_names = image_variables[j];
		}
		else {
		channel_names = channel_names + ";" + image_variables[j];
		}
	}
	//Start the channel splitter macro
	variables_to_pass_splitting = folders[0] + "," + folders [1] + "," + channel_names + "," + image_variables[0] + "," + is_silent_mode;
	runMacro("2 Channel splitter", variables_to_pass_splitting);
	
	//run the analyses and make sure they are only ran once each
	nuclei_analyzed = 0;
	IFT_analyzed = 0;
	cilia_analyzed = 0;
	
		//and if there is an IFT channel, only analyze ciliary length if this is indicated by the user
	if (is_analyze_length==0){
		cilia_analyzed = 1;
		nuclei_analyzed = 1;
	}
	
	for (k=1; k<image_variables.length; k++){
		
		//Determine the total number of nuclei per images using the other macros
		if(nuclei_analyzed == 0){
			if (startsWith(image_variables[k], "Nuclear marker")){
				nuclei_analyzed = 1;
				variables_to_pass_count_nuclei =  folders[1] + "," + is_silent_mode;
				runMacro("3 Counting nuclei", variables_to_pass_count_nuclei);
			}
		}
		//Measure the cilia per images using the other macros
		if(cilia_analyzed == 0){
			if (startsWith(image_variables[k], "Ciliary marker")){
				cilia_analyzed = 1;
				variables_to_pass_measure_cilia =  folders[1] + "," + is_silent_mode;
				runMacro("4 Measuring cilia length", variables_to_pass_measure_cilia);
			}
		}
		//Analyze the IFT accumulations per images using the other macros
		if(IFT_analyzed == 0){
			if (startsWith(image_variables[k], "IFT marker")){
				IFT_analyzed = 1;
				variables_to_pass_IFT =  folders[1] + "," + is_silent_mode;
				runMacro("5 Measuring IFT accumulations", variables_to_pass_IFT);
			}
		}	
	}
}

//Saves the summary and the log file of the run
function SaveLog(output_folder){
	selectWindow("Log");
	saveAs("txt", output_folder +"Log.txt");
}