# How to get the ALPACA toolbar in FIJI
1. Go to the FIJI Macros folder on your device. You can find this folder by opening FIJI and then select *File* > *Show folder* > *Macros*.
2. Unpack the .zip file in your *Macros* folder and make sure the files are unpacked directly into this folder.
3. Place the *Cilia Tools.ijm* file in the subfolder *Toolsets* that is already present in your *Macros* folder.
4. Start up FIJI and click the >>-icon on the right side of your toolbar. Choose the *Cilia Tool* menu. The bar will change automatically.
5. You can either run the full analyses by clicking the ALPACA-icon on the left or you can use the individual macros from the toolbar.
The ALPACA toolbar will remain available, even if you restart FIJI.

If you want to switch back to one of the FIJI toolbars, click the >>-icon again to choose another toolbar.
